<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WPDev4IM Shop Default Theme
 * @since WPDev4IM Shop Default Theme 1.0
 */
?>

<?php get_header(); ?>
<div id="mainBody">
	<div class="container">
		<div class="row">
			<?php get_sidebar('product' ); ?>

			<div class="span9">

				<?php

				if(function_exists('bcn_display')){
					echo ' <div class="breadcrumb">';
					bcn_display();
					echo ' </div>';
				}
				?>

				<div class="product-detail">

					<?php 
					$tags = array();
					while ( have_posts() ) : the_post();

					$meta = get_post_custom();

					$exclude_ids[]	= get_the_ID();

					$tags 						=  get_the_terms($post->ID,'product-tag');
					$productname 			    =  !empty($meta['productname'][0]) 				    ? $meta['productname'][0] : "";
					$merchantname 				=  !empty($meta['merchantname'][0]) 				? $meta['merchantname'][0] : "";
					$sku 						=  !empty($meta['sku'][0]) 							? $meta['sku'][0] : "";
					$category 					=  !empty($meta['category'][0]) 					? str_replace('~~',' &frasl; ',$meta['category'][0]) : "";
					$price 						=  !empty($meta['price'][0]) 						? wpdev4im_money_format($meta['price'][0]) : "";
					$product_short_description 	=  !empty($meta['product_short_description'][0]) 	? $meta['product_short_description'][0] : "";
					$linkurl 					=  !empty($meta['linkurl'][0]) 						? $meta['linkurl'][0] : "";
					$imageurl 					=  !empty($meta['imageurl'][0]) 					? $meta['imageurl'][0] : "";
					$rating 					=  !empty($meta['rating'][0]) 					    ? $meta['rating'][0] : "";
					$reviews 					=  !empty($meta['reviews'][0]) 					    ? $meta['reviews'][0] : "";

					$tags_list = get_the_term_list( $post->ID, 'product-tag', __( '', 'wpdev4im' ), __( ', ', 'wpdev4im' ), '' );

					?>


					<div class="row">	  
						<div id="gallery" class="span3">
							<?php if($imageurl ){ ?>
							<img src="<?php  echo $imageurl ; ?>" alt="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'wpdev4im' ), the_title_attribute( 'echo=0' ) ) ); ?>">
							<?php } ?>
						</div>

						<div class="span6">
							<h3 class="product-title"><?php the_title( ); ?></h3>
							
							<div itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">					    
								<img   alt="Overall Rating" src="http://ak1.ostkcdn.com/img/mxc/stars<?=str_replace('.', '_', $rating)?>.gif" width="70" height="13"> Rating
								<span itemprop="ratingValue"><?=$rating?></span>
								<span class="ratingsSep">&nbsp;|&nbsp;</span>
								<a id="readReviewsLink" title="Read Reviews" href="#custreviews">
									<span itemprop="reviewCount"><?=$reviews?></span> reviews
								</a>
								<span class="ratingsSep">&nbsp;|&nbsp;</span>
								<a id="writeReviewLink" title="Write Review" href="#writereviews" rel="nofollow">Write a review</a>					    
							</div>

							<p><strong>Categories:</strong><small> <?php echo $category ; ?></small></p>
							<hr class="soft"/>
							
							<div class="control-group">

								<div class="controls">
									<span class="price"><?php echo  $price; ?></span>

									<a class="btn btn-large btn-primary pull-right" href="<?php echo  esc_url($linkurl); ?>">Buy now<i class="icon-shopping-cart "></i></a> 
								</div>
							</div>
							

							<hr class="soft"/>

							<p>
								<?php echo  $product_short_description; ?>

							</p>
							<?php if($tags_list){ ?>
							<div class="tabgs">
								<span class="tags-links">
									<?php printf( __( '<strong>Tagged</strong> : %1$s', 'wpdev4im' ), $tags_list ); ?>
								</span>

							</div>
							<hr class="soft"/>
							<?php } ?>
						</div>
					</div>
					<div class="span9">

						<div class="product-description" id="home">
							<h4><?php _e('Product Information', 'wpdev4im' ); ?></h4>
							<?php the_content( ); ?>
						</div>

						<hr class="soft"/>

						<div class="product-relate" id="profile">

							<div class="tab-pane" id="blockView">

								<h4><?php _e( 'Related products', 'wpdev4im' ); ?></h4>
								<div class="row-fluid">
									<div id="featured" class="carousel slide">
										<div class="carousel-inner">
											<div class="item active">
												<ul class="thumbnails">
													<?php

													$tag_ids = array();

													foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;


													$args = array(
														'post_type' 		=> 'product',
														'orderby' 			=> 'rand',
														'order' 			=> 'ASC',
														'posts_per_page' 	=> '4',
														'post__not_in' 		=> $exclude_ids,
														'tax_query' 		=> array(
															array(
																'taxonomy' => 'product-tag',
																'field' => 'id',
																'terms' => $tag_ids
																)
															)
														);

													$query = new WP_Query( $args );

													if ( $query->have_posts() ) {
														while ( $query->have_posts() ) { $query->the_post(); ?>

														<li class="span3">
															<?php  get_template_part( 'loop', 'product' ); ?>
														</li>

														<?php
													} /* endwhile */
												}
												/* Restore original Post Data */
												wp_reset_postdata();

												?>
											</ul>

										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					<br class="clr">
				</div>

			<?php endwhile; // end of the loop. ?>

		</div>
	</div>

</div>
</div>
</div>
</div>
<!-- MainBody End ============================= -->
<?php get_footer(); ?>