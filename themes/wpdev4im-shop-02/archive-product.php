<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WPDev4IM Shop Default Theme
 * @since WPDev4IM Shop Default Theme 1.0
 */
?>

<?php get_header(); ?>
<div id="mainBody">
	<div class="container">
		<div class="row">
			<?php get_sidebar('product' ); ?>
			<div class="span9">


				<?php
				if(function_exists('bcn_display')){
					echo ' <div class="breadcrumb">';
					bcn_display();
					echo ' </div>';
				}
				?>

				
				<h3> Products<!--  <small class="pull-right">  <?php //echo wp_count_posts('product'); ?>  products are available </small> --></h3>	
				<hr class="soft"/>
				

				<br class="clr"/>


				<div class="tab-pane  active" id="blockView">
					<ul class="thumbnails">
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<li class="span3">
								<?php  get_template_part( 'loop', 'product' ); ?>
							</li>
						<?php endwhile; ?>
					<?php else: ?>
						<?php get_template_part( 'no-results', 'index' ); ?>
					<?php endif; ?>
				</ul>
				<hr class="soft"/>
			</div>
		</div>


		<?php wpdev4im_content_nav( 'nav-below' ); ?>

		<br class="clr"/>
		</div>
	</div>
</div>

<!-- MainBody End ============================= -->
<?php get_footer(); ?>