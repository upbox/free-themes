<?php
/**
 * The template for displaying search forms in WPDev4IM Shop Default Theme
 *
 * @package WPDev4IM Shop Default Theme
 * @since WPDev4IM Shop Default Theme 1.0
 */
?>
	<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
		<input type="text" class="field input-medium search-query" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" id="s" placeholder="<?php esc_attr_e( 'Search &hellip;', 'wpdev4im' ); ?>" />
		<input type="submit" class="submit btn .btn-default" name="submit" id="searchsubmit" value="<?php esc_attr_e( 'Search', 'wpdev4im' ); ?>" />
	</form>
