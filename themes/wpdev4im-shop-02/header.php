<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WPDev4IM Shop Default Theme
 * @since WPDev4IM Shop Default Theme 1.0
 */
?>

<?php /*
<!DOCTYPE html>
<html <?php language_attributes(); ?> >
	<head>
    	<meta charset="<?php bloginfo( 'charset' ); ?>" />
    	<title><?php wp_title( '|', true, 'right' ); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
		<?php if(wpdev4im_get_option('favicon')) { ?>
        <link rel="icon" type="image/png" href="<?php echo wpdev4im_get_option('favicon'); ?>">
        <?php } ?>
        
        <!-- Bootstrap -->
    	<link href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/font-awesome.css" rel="stylesheet" media="screen">
        
        <?php wp_head(); ?>
    
    </head>
    
	<body <?php body_class(); ?>>
    
        <div class="navbar navbar-inverse navbar-fixed-top">
        
        	<div class="container">
        		<div class="navbar-header">
                	
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
          
                  <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
                    <?php if(wpdev4im_get_option('logo') and wpdev4im_get_option('logo-style')) { ?>
                        <img src="<?php echo wpdev4im_get_option('logo'); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"> 
                    <?php }else{ ?>
                            <?php bloginfo( 'name' );   ?>
                    <?php } ?>
                  </a>
          		</div><!-- /.navbar-header -->
                
              	<div class="collapse navbar-collapse pull-left">
                	<?php wp_nav_menu( array( 'theme_location' => 'primary','menu_class' => 'nav navbar-nav','container' => 'ul' , 'walker' => new wp_bootstrap_navwalker(),'fallback_cb' => '' )); ?>
              	</div><!--/.navbar-collapse -->
                
                <ul class="social-network icons-ul pull-right list-inline">
					          <?php if(wpdev4im_get_option("facebook")){ ?>  	<li><a rel="nofollow" target="_blank" href="<?php echo wpdev4im_get_option("facebook"); ?>"><i class="fontawesome-icon icon-facebook"></i></a></li><?php } ?>
                  	<?php if(wpdev4im_get_option("twitter")){ ?>  	<li><a rel="nofollow" target="_blank" href="<?php echo wpdev4im_get_option("twitter"); ?>"><i class="fontawesome-icon icon-twitter"></i></a></li><?php } ?>
                    <?php if(wpdev4im_get_option("googleplus")){ ?> 	<li><a rel="nofollow" target="_blank" href="<?php echo wpdev4im_get_option("googleplus"); ?>"><i class="fontawesome-icon  icon-google-plus"></i></a></li><?php } ?>
                  	<?php if(wpdev4im_get_option("instagram")){ ?>  	<li><a rel="nofollow" target="_blank" href="<?php echo wpdev4im_get_option("instagram"); ?>"><i class="fontawesome-icon icon-instagram"></i></a></li><?php } ?>
                  	<?php if(wpdev4im_get_option("skype")){ ?>  		<li><a rel="nofollow" target="_blank" href="<?php echo wpdev4im_get_option("skype"); ?>"><i class="fontawesome-icon icon-skype"></i></a></li><?php } ?>
                  	<?php if(wpdev4im_get_option("github")){ ?>  		<li><a rel="nofollow" target="_blank" href="<?php echo wpdev4im_get_option("github"); ?>"><i class="fontawesome-icon icon-github"></i></a></li><?php } ?>
                  	<?php if(wpdev4im_get_option("linkedin")){ ?>  	<li><a rel="nofollow" target="_blank" href="<?php echo wpdev4im_get_option("linkedin"); ?>"><i class="fontawesome-icon icon-linkedin"></i></a></li><?php } ?>
                </ul><!--/.social-icons-->
            
        	</div><!-- /.container -->
        </div><!-- /.navbar -->

        <?php */ ?>        
        
        <!DOCTYPE html>
        <html <?php language_attributes(); ?> >
        <head>
          <meta charset="<?php bloginfo( 'charset' ); ?>" />
          <title><?php wp_title( '|', true, 'right' ); ?></title>

          <meta name="viewport" content="width=device-width, initial-scale=1.0">


          <!-- Bootstrap style --> 
          <link id="callCss" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/themes/bootshop/bootstrap.min.css" media="screen"/>
          <link href="<?php echo get_template_directory_uri(); ?>/themes/css/base.css" rel="stylesheet" media="screen"/>
          <!-- Bootstrap style responsive --> 
          <link href="<?php echo get_template_directory_uri(); ?>/themes/css/bootstrap-responsive.min.css" rel="stylesheet"/>
          <link href="<?php echo get_template_directory_uri(); ?>/themes/css/font-awesome.css" rel="stylesheet" type="text/css">
          <!-- Google-code-prettify --> 
          <link href="<?php echo get_template_directory_uri(); ?>/themes/js/google-code-prettify/prettify.css" rel="stylesheet"/>
          <!-- fav and touch icons -->
          <link rel="shortcut icon" href="themes/images/ico/favicon.ico">
          <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/themes/images/ico/apple-touch-icon-144-precomposed.png">
          <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/themes/images/ico/apple-touch-icon-114-precomposed.png">
          <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/themes/images/ico/apple-touch-icon-72-precomposed.png">
          <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/themes/images/ico/apple-touch-icon-57-precomposed.png">
          <style type="text/css" id="enject"></style>

         <?php wp_head(); ?>
         
        </head>
        <body>
          <div id="header">

            <div class="container">

              <div id="welcomeLine" class="row">
             <!--    <div class="span6">Welcome!<strong> User</strong></div>
                <div class="span6">
                  <div class="pull-right">
                    <a href="product_summary.html"><span class="">Fr</span></a>
                    <a href="product_summary.html"><span class="">Es</span></a>
                    <span class="btn btn-mini">En</span>
                    <a href="product_summary.html"><span>&pound;</span></a>
                    <span class="btn btn-mini">$155.00</span>
                    <a href="product_summary.html"><span class="">$</span></a>
                    <a href="product_summary.html"><span class="btn btn-mini btn-primary"><i class="icon-shopping-cart icon-white"></i> [ 3 ] Itemes in your cart </span> </a> 
                  </div>
                </div> -->
              </div> 

              <!-- Navbar ================================================== -->
              <div id="logoArea" class="navbar">
                <a id="smallScreen" data-target="#topMenu" data-toggle="collapse" class="btn btn-navbar">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </a>
                <div class="navbar-inner">

                  <a class="brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
                    <?php if(wpdev4im_get_option('logo') and wpdev4im_get_option('logo-style')) { ?>
                    <img src="<?php echo wpdev4im_get_option('logo'); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"> 
                    <?php }else{ ?>
                    <?php bloginfo( 'name' );   ?>
                    <?php } ?>
                  </a>

   <!--  <form class="form-inline navbar-search" method="post" action="products.html" >
    <input id="srchFld" class="srchTxt" type="text" />
      <select class="srchTxt">
      <option>All</option>
      <option>CLOTHES </option>
      <option>FOOD AND BEVERAGES </option>
      <option>HEALTH & BEAUTY </option>
      <option>SPORTS & LEISURE </option>
      <option>BOOKS & ENTERTAINMENTS </option>
    </select> 
      <button type="submit" id="submitButton" class="btn btn-primary">Go</button>
    </form> -->


                  <?php wp_nav_menu( array( 'theme_location' => 'primary','menu_id'=>'topMenu','menu_class' => 'nav pull-right','container' => 'ul' , 'walker' => new wp_bootstrap_navwalker(),'fallback_cb' => '' )); ?>


                </div>
              </div>
            </div>
          </div>
<!-- Header End====================================================================== -->