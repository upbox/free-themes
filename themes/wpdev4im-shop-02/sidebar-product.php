 <!-- Sidebar ================================================== -->
      <div id="sidebar" class="span3">

      <?php if ( ! dynamic_sidebar( 'sidebar-product' ) ) : ?>
    	

       <!--  <div class="well well-small">
          <a id="myCart" href="product_summary.html">
            <img src="<?php echo get_template_directory_uri(); ?>/themes/images/ico-cart.png" alt="cart">
            3 Items in your cart  <span class="badge badge-warning pull-right">$155.00</span>
          </a>
        </div> -->
        
        <ul id="sideManu" class="nav nav-tabs nav-stacked">
          <?php

          $term_args = array(
            'orderby'       => 'name', 
            'order'         => 'ASC',
            'hide_empty'    => true, 
            'parent'        => '',
            'hierarchical'  => true, 
            'pad_counts'    => true, 
            ); 

          $terms = get_terms("product-category", $term_args);
          if ( !empty( $terms ) && !is_wp_error( $terms ) ){
           foreach ( $terms as $term ) {
             echo '<li><a href="' . get_term_link( $term ) . '" title="'.$term->name. '"><i class="icon-chevron-right"></i> ' . $term->name . ' ('.$term->count.')</a></li>';

           }
         }
         ?>

         <!-- 

         <li class="subMenu open"><a> ELECTRONICS [230]</a>
          <ul>
            <li><a class="active" href="products.html"><i class="icon-chevron-right"></i>Cameras (100) </a></li>
            <li><a href="products.html"><i class="icon-chevron-right"></i>Computers, Tablets & laptop (30)</a></li>
            <li><a href="products.html"><i class="icon-chevron-right"></i>Mobile Phone (80)</a></li>
            <li><a href="products.html"><i class="icon-chevron-right"></i>Sound & Vision (15)</a></li>
          </ul>
        </li>
      
        <li><a href="products.html">BOOKS & ENTERTAINMENTS [14]</a></li> -->
      </ul>
      <br/>

      <?php

      // Random 2 Product

      $args = array(
        'post_type'       => 'product',
        'orderby'         => 'rand',
        'posts_per_page'  => 2
        );

      $query = new WP_Query( $args );

      if ( $query->have_posts() ) {

        while ( $query->have_posts() ) {  $query->the_post();

          get_template_part( 'loop', 'product' );
          echo '<br>';

        }
      }

      /* Restore original Post Data */
      wp_reset_postdata();

      ?>

        <?php endif; // end sidebar widget area ?>

    </div>
    <!-- Sidebar end=============================================== -->