<?php
/**
 * The Template for displaying all single posts type.
 *
 * @package WPDev4IM Shop Default Theme
 * @since WPDev4IM Shop Default Theme 1.0
 */

get_header(); ?>

	<?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <?php
              if ( has_post_thumbnail() ) {
                    the_post_thumbnail();
              }else{
                    echo '<img src="' . get_bloginfo( 'stylesheet_directory' ) . '/img/thumbnail-default.png" />';
              }
            ?>
            <?php get_template_part( 'loop', 'index' ); ?>
        <?php endwhile; ?>
        <?php wpdev4im_content_nav( 'nav-below' ); ?>
    <?php else : ?>
        <?php get_template_part( 'no-results', 'index' ); ?>
    <?php endif; ?>

	<?php get_sidebar(); ?>

<?php get_footer(); ?>