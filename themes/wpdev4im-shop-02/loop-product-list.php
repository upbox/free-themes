<?php
$meta = get_post_custom();
$exclude_ids[] = get_the_ID();

$merchantname               =  !empty($meta['merchantname'][0])         ? $meta['merchantname'][0] : "";
$sku                        =  !empty($meta['sku'][0])              ? $meta['sku'][0] : "";
$category                   =  !empty($meta['category'][0])           ? str_replace('~~',' &frasl; ',$meta['category'][0]) : "";
$price                      =  !empty($meta['price'][0])            ? wpdev4im_money_format($meta['price'][0]) : "";
$product_short_description  =  !empty($meta['product_short_description'][0])  ? $meta['product_short_description'][0] : "";
$linkurl                    =  !empty($meta['linkurl'][0])            ? $meta['linkurl'][0] : "";
$imageurl                   =  !empty($meta['imageurl'][0])           ? $meta['imageurl'][0] : "";

?>


<div class="row">	  
	<div class="span2">
		<img src="<?php  echo $imageurl ; ?>" alt="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'wpdev4im' ), the_title_attribute( 'echo=0' ) ) ); ?>">
	</div>
	<div class="span4">
	
	<!--<h3>New | Available</h3>
	<hr class="soft"/>-->		
		
		<h5><?php the_title(); ?></h5>
		<p>
			<?php echo $product_short_description ; ?>
		</p>
		<a class="btn btn-small" href="<?php get_permalink(); ?>">View Details</a>
		<br class="clr"/>
	</div>
	<div class="span3 alignR">
		
			<h3><?php echo $price; ?></h3>
			
<a class="btn" href="<?php the_permalink(); ?>"> <i class="icon-zoom-in"></i></a> 
				<a class="btn" href="<?php echo  esc_url($linkurl); ?>">Buy now<i class="icon-shopping-cart"></i></a> 
				

	
	</div>
</div>
<hr class="soft"/>