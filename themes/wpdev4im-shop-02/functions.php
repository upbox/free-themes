<?php
/**
 * WPDev4IM Shop Default Theme functions and definitions
 *
 * @package WPDev4IM Shop Default Theme
 * @since WPDev4IM Shop Default Theme 1.0
 */


/**
 * Set the content width based on the theme's design and stylesheet.
 * 
 * @since WPDev4IM Shop Default Theme 1.0
 * Doc http://codex.wordpress.org/Content_Width
 */
if ( ! isset( $content_width ) )
	$content_width = 640; /* pixels */

/*
 * Load Theme Options
 */
require( get_template_directory() . '/admin/theme-options.php' );


/*
 * Get Theme Option by WPDev4IM Shop option id
 */
function wpdev4im_get_option($wpdev4im_option_id = ""){
	return do_shortcode('[wpdev4im_option id="'.$wpdev4im_option_id.'"]');
}


/*
 * Load Post Type  compatibility file.
 */
//require( get_template_directory() . '/admin/post-type-slideshow.php' );
//require( get_template_directory() . '/admin/post-type-custom.php' );

require( get_template_directory() . '/admin/post-type-product.php' );


/*
 * Load WPDev4IM Shop Custom Custom-Metaboxes-and-Fields compatibility file.
 */
require( get_template_directory() . '/admin/custom-meta-boxes.php' );


if ( ! function_exists( 'wpdev4im_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 * @since WPDev4IM Shop Default Theme 1.0
 */
function wpdev4im_setup() {

	/**
	 * Custom template tags for this theme.
	 */
	require( get_template_directory() . '/lib/template-tags.php' );
	
	/**
	 * Custom functions that wp_bootstrap_navwalker independently of the theme templates
	 */
	require( get_template_directory() . '/lib/wp_bootstrap_navwalker.php' );
	
	/**
	 * Custom functions that act independently of the theme templates
	 */
	require( get_template_directory() . '/lib/extras.php' );
	
	/**
	 * Make theme available for translation
	 * Translations can be filed in the /lin/ directory
	 * If you're building a theme based on WPDev4IM Shop Default Theme, use a find and replace
	 * to change 'wpdev4im' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'wpdev4im', get_template_directory() . '/lang' );

	/**
	 * Add default posts and comments RSS feed libks to head
	 */
	add_theme_support( 'automatic-feed-libks' );

	/**
	 * Enable support for Post Thumbnails
	 * Doc http://codex.wordpress.org/Function_Reference/add_theme_support
	 */
	add_theme_support( 'post-thumbnails' );
	
	/*
	 * Doc http://codex.wordpress.org/Function_Reference/add_image_size
	 */
	 //add_image_size( 'homepage-thumb', 220, 180, true ); 

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'wpdev4im' ),
		'footer' => __( 'Footer Menu', 'wpdev4im' ),
	) );

	/**
	 * Enable support for Post Formats
	 */
	//add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'libk' ) );
}
endif; // wpdev4im_setup

add_action( 'after_setup_theme', 'wpdev4im_setup' );

/**
 * Setup the WordPress core custom background feature.
 *
 * Use add_theme_support to register support for WordPress 3.4+
 * as well as provide backward compatibility for WordPress 3.3
 * using feature detection of wp_get_theme() which was introduced
 * in WordPress 3.4.
 *
 * @todo Remove the 3.3 support when WordPress 3.6 is released.
 *
 * Hooks into the after_setup_theme action.
 * 
 * Doc http://codex.wordpress.org/Custom_Backgrounds
 * Generater tools http://generatewp.com/theme-support/
 */
function wpdev4im_register_custom_background()  {
	global $wp_version;

	// Add theme support for Custom Background
	$background_args = array(
		'default-color'          => 'ffffff',
		'default-image'          => '',
		'wp-head-callback'       => '_custom_background_cb',
		'admin-head-callback'    => '',
		'admin-preview-callback' => '',
	);
	if ( version_compare( $wp_version, '3.4', '>=' ) ) :
		add_theme_support( 'custom-background', $background_args );
	else :
		add_custom_background();
	endif;
}

// Hook into the 'after_setup_theme' action
add_action( 'after_setup_theme', 'wpdev4im_register_custom_background' );


/**
 * Register widgetized area and update sidebar with default widgets
 * @since WPDev4IM Shop Default Theme 2.0
 */
function wpdev4im_widgets_init() {
	
	register_sidebar( array(
		'name' => __( 'Sidebar Default', 'wpdev4im' ),
		'id' => 'sidebar-default',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Sidebar Product', 'wpdev4im' ),
		'id' => 'sidebar-product',
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );
	
	
	register_sidebar( array(
		'name' => __( 'Sidebar Footer', 'wpdev4im' ),
		'id' => 'sidebar-footer',
		'before_widget' => '<div id="%1$s" class="span3 %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5>',
		'after_title' => '</h5>',
	) );
	
}
add_action( 'widgets_init', 'wpdev4im_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function wpdev4im_scripts() {
	
	
	//wp_enqueue_style( 'my-theme', get_template_directory_uri() . '/assets/css/style.css' );

	wp_enqueue_style( 'style', get_stylesheet_uri() );	
	
	//wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.js', array( 'jquery' ), '1.0', true );
	//wp_enqueue_script( 'html5', get_template_directory_uri() . '/assets/js/html5.js', array( 'jquery' ), '1.0', true );
	
	wp_enqueue_script( 'wpdev4im-custom', get_template_directory_uri() . '/assets/js/theme-script.js', array( 'jquery' ), '1.0', true );
	

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'wpdev4im_scripts' );

/**
 * Implement the Custom Header feature
 */
//require( get_template_directory() . '/lib/custom-header.php' );


/**
 * Custom copyright text.
 */
function wpdev4im_copyright(){
	echo "&copy; ".__( "Copyright", 'wpdev4im' )." " . get_bloginfo( 'name' )." ". date("Y ") .". ".__( "All rights reserved.", 'wpdev4im' );
}
 add_action('wpdev4im_copyright','wpdev4im_copyright');


/**
 * Control Excerpt Length using Filters
 * http://codex.wordpress.org/Function_Reference/the_excerpt
 */
function wpdev4im_custom_excerpt_length( $length ) {
	return 50;
}
add_filter( 'excerpt_length', 'wpdev4im_custom_excerpt_length', 999 );

/**
 * Make the "read more" libk to the post
 * http://codex.wordpress.org/Function_Reference/the_excerpt
 */
function wpdev4im_new_excerpt_more($more) {
    global $post;
	return ' <a class="readmore" href="'. get_permalink($post->ID) . '">'.__( 'Read More...', 'wpdev4im' ).'</a>';
}
add_filter('excerpt_more', 'wpdev4im_new_excerpt_more');



function wpdev4im_rewrite_flush() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'wpdev4im_rewrite_flush' );



function wpdev4im_money_format($money=0){
	
	return "$". number_format(intval($money),2, '.', ',');
	
}



function wpdev4im_widget_categories_args_filter( $cat_args ) {
      
	  $queried_post_type = get_query_var('post_type');
	  
       if(is_tax( 'product-tag') or is_tax( 'product-category') or ($queried_post_type=='post')){
	   	$cat_args['taxonomy']='product-category';
	   }
	   
       return $cat_args;
}
add_filter( 'widget_categories_args', 'wpdev4im_widget_categories_args_filter', 10, 1 );
