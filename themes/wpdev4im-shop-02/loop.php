
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
    <h2 class="title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'wpdev4im' ); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>

	<div class="meta"> 
  		<?php wpdev4im_posted_on(); ?>
		<?php
             /* Doc : http://codex.wordpress.org/Function_Reference/get_the_category_list */
             $categories_list = get_the_category_list( __( ', ', 'wpdev4im' ) );
             if ( $categories_list) :
             ?>
             <span class="categories">
             <?php printf( __( 'Posted in %1$s', 'wpdev4im' ), $categories_list ); ?>
             </span>
             <?php endif; // End if categories ?>
             
            <?php
             /* Doc : http://codex.wordpress.org/Function_Reference/get_the_tag_list */
             $tags_list = get_the_tag_list( '', __( ', ', 'wpdev4im' ) );
             if ( $tags_list ) :
             ?>
             <span class="sep"> | </span>
             <span class="tags-links">
             <?php printf( __( 'Tagged %1$s', 'wpdev4im' ), $tags_list ); ?>
             </span>
             <?php endif; // End if $tags_list ?>
             
            <?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
             <span class="sep"> | </span>
             <span class="comments"><?php comments_popup_link( __( 'Leave a comment', 'wpdev4im' ), __( '1 Comment', 'wpdev4im' ), __( '% Comments', 'wpdev4im' ) ); ?></span>
		<?php endif; ?>

        <hr class="soft"/>
        
	</div> <!--meta --> 


    
    <div class="entry">
		<?php 
            if ( is_singular() ) {
                 the_content();
            }else{
				// Doc http://codex.wordpress.org/Function_Reference/the_post_thumbnail
				if ( has_post_thumbnail() ) { 
				  the_post_thumbnail('thumbnail');
				} 
                the_excerpt();
            }
        ?>
  	</div>
    
</div> <!-- post -->