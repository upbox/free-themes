<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WPDev4IM Shop Default themes
 * @since WPDev4IM Shop Default themes 1.0
 */
?>

<?php get_header(); ?>

	<?php /* ?>
		<?php if (have_posts()) : ?>
                
        	<?php while (have_posts()) : the_post(); ?>
                <div class="page-header">
                	<h1><?php the_title(); ?></h1>
                </div>

                <div class="page-content">
					<?php the_content(); ?>
                </div>
            <?php endwhile; ?>
              
            <?php wpdev4im_content_nav( 'nav-below' ); ?>
             
        <?php else : ?>
            
        	<?php get_template_part( 'no-results', 'index' ); ?>
            
   		<?php endif; ?>
        

   
   <hr />
   
	<div class="row">
   		<?php dynamic_sidebar( 'sidebar-front-page' ) ; ?>
	</div>
  <?php */ ?>


  <div id="carouselBlk">
    <div id="myCarousel" class="carousel slide">
      <div class="carousel-inner">

        <?php 
        $args = array(
          'post_type' => 'product',
          'posts_per_page' => '4'
          );
        
        $query = new WP_Query( $args );
        $i = 0;
            // The Loop
        if ( $query->have_posts() ) {
          while (  $query->have_posts() ) :  $query->the_post();
          
          $meta = get_post_custom();
               // print_r( $meta);

          $merchantname               =  !empty($meta['merchantname'][0])         ? $meta['merchantname'][0] : "";
          $sku                        =  !empty($meta['sku'][0])              ? $meta['sku'][0] : "";
          $category                   =  !empty($meta['category'][0])           ? str_replace('~~',' &frasl; ',$meta['category'][0]) : "";
          $price                      =  !empty($meta['price'][0])            ? wpdev4im_money_format($meta['price'][0]) : "";
          $product_short_description  =  !empty($meta['product_short_description'][0])  ? $meta['product_short_description'][0] : "";
          $linkurl                    =  !empty($meta['linkurl'][0])            ? $meta['linkurl'][0] : "";
          $imageurl                   =  !empty($meta['imageurl'][0])           ? $meta['imageurl'][0] : "";
          $rating                     =  !empty($meta['rating'][0])               ? $meta['rating'][0] : "";
          $reviews                    =  !empty($meta['reviews'][0])              ? $meta['reviews'][0] : "";

          $exclude_ids[]              = get_the_ID();


          $class = "";

          if( $i ==0 ) $class= 'active';

          $i++;


          ?>

          <div class="item <?php echo $class; ?>">
            <div class="container">
              <div class="row">

                <div class="span4">
                  <a href="<?php the_permalink(); ?>">
                    <img src="<?php  echo $imageurl ; ?>" alt="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'wpdev4im' ), the_title_attribute( 'echo=0' ) ) ); ?>">
                  </a>
                </div>

                <div class="span8">
                  <div class="carousel-captionx text-left">
                    <h3><?php the_title(); ?></h3>
                    <div class="description">
                      <p><?php echo  $product_short_description; ?></p>
                    </div>
                    
                    
                    <div class="pull-left">
                      <div class="price"> <span class="big"><?php echo  $price; ?></span>

                        <a href="<?php echo esc_url($linkurl); ?>" class="btn btn-large btn-primary">Buy Now</a>
                      </div>
                    </div>

                  </div>
                </div>

                <div class="clear"></div>

              </div>
            </div>
          </div>


        <?php endwhile;
      }  ?>

    </div>
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
  </div> 
</div>


<div id="mainBody">
  <div class="container">
    <div class="row">

      <?php get_sidebar('product'); ?>

      <div class="span9">     

        <h4>Latest Products </h4>
        <ul class="thumbnails">

          <?php


          $args = array(
            'post_type' => 'product',
            'post__not_in' => $exclude_ids
            );

          $query = new WP_Query( $args );

          // The Loop
          if ( $query->have_posts() ) {

            while ( $query->have_posts() ) {  $query->the_post();
              echo '<li class="span3">';
              get_template_part( 'loop', 'product' );
              echo '</li>';

            }

            wpdev4im_content_nav( 'nav-below' );

          }else{
           get_template_part( 'no-results', 'index' );
         }

         /* Restore original Post Data */
         wp_reset_postdata();

         ?>

       </ul> 

     </div>
   </div>
 </div>
</div>  
<?php get_footer(); ?>
