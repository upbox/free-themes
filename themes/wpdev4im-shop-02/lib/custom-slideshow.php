	<?php

		$args = array(
			'post_type'		 	=> 'slideshow',
			'orderby' 			=> 'menu_order',
			'order'				=> 'ASC',
			'posts_per_page' 	=> -1
		);
		$the_query = new WP_Query( $args );
		
		if($the_query->post_count>0){ ?>
        
            <div id="_sCarousel" class="carousel slide">
          		<div class="carousel-inner">
                    <?php
					
						$i = 0;
                        while ( $the_query->have_posts() ) : $the_query->the_post();
                            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ),'full');
                            
                            ?>
                              <div id="item-<?php echo the_ID(); ?>" class="item <?php if($i===0){ echo 'active'; } $i++; ?>" style="background:url(<?php echo $image[0];?>) center center no-repeat">
                              	<div class="container">
                                	<?php the_content(); ?>
                                </div>
                              </div>
                              
                            <?php
                            
                        endwhile;
							
                    ?>
                  </div><!-- .carousel-inner -->
              <a class="left carousel-control" href="#_sCarousel" data-slide="prev">&lsaquo;</a>
              <a class="right carousel-control" href="#_sCarousel" data-slide="next">&rsaquo;</a>
            </div><!-- /.carousel -->
          
      <?php }else{  ?>
        
        <!-- Carousel
        ================================================== -->
        <div id="_sCarousel" class="carousel slide">
          <div class="carousel-inner">
            <div class="item active">
              <img alt="" src="<?php echo get_template_directory_uri(); ?>/img/slider.png" />
              <div class="container">
              &nbsp;
              </div>
            </div>
            <div class="item">
              <img alt="" src="<?php echo get_template_directory_uri(); ?>/img/slider.png" />
              <div class="container">
               &nbsp;
              </div>
            </div>
            <div class="item">
              <img alt="" src="<?php echo get_template_directory_uri(); ?>/img/slider.png" />
               <div class="container">
               &nbsp;
              </div>
            </div>
          </div>
          <a class="left carousel-control" href="#_sCarousel" data-slide="prev">&lsaquo;</a>
          <a class="right carousel-control" href="#_sCarousel" data-slide="next">&rsaquo;</a>
        </div><!-- /.carousel -->
        
        <?php } //END IF ?>
        
		<?php wp_reset_postdata();?>
   
   
