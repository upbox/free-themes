<!-- Custom_Post_Type -->
    
	<?php
	
	     // Doc :  http://codex.wordpress.org/Class_Reference/WP_Query
		 
		$args = array(
				'post_type'		 	=> 'CUSTOM_POST_TYPE',
				'orderby' 			=> 'menu_order',
				'order'				=> 'ASC',
				'posts_per_page' 	=> 3
			);
		$the_query = new WP_Query( $args );
	
		if($the_query->post_count>0){
	
		$i		= 0;
		$col 	= 3;
	
		while ( $the_query->have_posts() ) : $the_query->the_post();
	
		if($i%$col===0){ echo ' <div class="row">';}
	
		?>
	
		<div class="col-md-<?php echo (12/$col); ?> <?php echo get_post_type(); ?>">
	
		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute('echo=0'); ?>" rel="bookmark">
		<?php
		if (has_post_thumbnail()) {
			the_post_thumbnail('medium', array('class' => ''));
		} else {
			echo '<img alt="" src="' . get_template_directory_uri() . '/img/thumbnail-default.png">';
		}
		?>
		</a>
	
		<h3 class="<?php echo get_post_type(); ?>-title"><?php the_title(); ?></h3>
	
		<div class="<?php echo get_post_type(); ?>-summary">
		<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
	
		</div>
	
		<?php
	
		if ($i % $col === ($col - 1)) { echo '</div><!-- /.row -->';
		}
	
		$i++;

	endwhile;
	?>
    
	<?php if((($i-1)%$col)!=($col-1)){ echo '</div><!-- /.row -->';} ?>
	
    <?php
	}
	/* 
	 * Restore original Post Data
	 */
	wp_reset_postdata();
	?>

<!-- /.custom_post_types -->
