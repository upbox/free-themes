<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WPDev4IM Shop Default Theme
 * @since WPDev4IM Shop Default Theme 1.0
 */
?>

<?php get_header(); ?>
<div id="mainBody">
    <div class="container">
        <div class="row">
            <?php get_sidebar('product' ); ?>
            <div class="span9">


                <?php

                if(function_exists('bcn_display')){
                    echo ' <div class="breadcrumb">';
                    bcn_display();
                    echo ' </div>';
                }
                ?>

                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                   <h3 class="page-title"><?php the_title( ); ?></h3> 
                   <hr class="soft"/>

                   <?php the_content( ); ?>

               <?php endwhile; endif; ?>

               <br class="clr" />

           </div>
       </div>
   </div>
</div>

<!-- MainBody End ============================= -->
<?php get_footer(); ?>