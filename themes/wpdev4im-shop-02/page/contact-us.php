<?php
/**
 * Template Name: Contact us
 *
 * This is the template that displays Full-width pages.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WPDev4IM Shop Default Theme
 * @since WPDev4IM Shop Default Theme 1.0
 */


get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
	
    	<div id="gMap"></div>
   
		<?php
            $address = esc_attr(strip_tags(do_shortcode('[wpdev4im_option id="address"]')));
            $address = str_replace("\r", '',  $address);   // --- replace with empty space
            $address = str_replace("\n", ' ', $address);   // --- replace with space
            $address = str_replace("\t", ' ', $address);   // --- replace with space
            
            $lat 	= do_shortcode('[wpdev4im_option id="map_lat"]');
            $lng 	= do_shortcode('[wpdev4im_option id="map_lng"]');
            $zoom 	= do_shortcode('[wpdev4im_option id="map_zoom"]');
        ?>
        <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
        <script type="text/javascript">
            /* <![CDATA[ */
              function initialize() {
                  var myLatlng = new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>);
                  var mapOptions = {
                    center: myLatlng,
                    zoom: <?php echo $zoom; ?>,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    mapTypeControl:true,
                    zoomControl: true,
                    zoomControlOptions: {
                      style: google.maps.ZoomControlStyle.SMALL
                    },
                    scrollwheel: false
                  };
                  var map = new google.maps.Map(document.getElementById("gMap"), mapOptions); 
                  
                  var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title:"<?php echo $address;  ?>"
                   // icon:"<?php echo get_template_directory_uri(); ?>/images/map-marker.png"
                });
              }
                google.maps.event.addDomListener(window, 'load', initialize);
            /* ]]> */
        </script>
            
        <div class="row">
            <div class="col-md-8">
             <?php the_content(); ?>
            </div>
            <div class="col-md-4 sidebar">   
              <div class="widget">
                  <h3><?php _e("Contact info","WPDev4IM Shop"); ?></h3>
                  <?php echo wpdev4im_get_option("address"); ?>
              </div><!--/sidebar wrap-->
              
              <div class="widget">
                  <?php echo wpdev4im_get_option("contact-info"); ?>
                  <p class="contact-number"><?php echo wpdev4im_get_option("phone"); ?></p>
              </div><!--/sidebar wrap-->
              
          </div><!--/sidebar-->
        </div>
 
	<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
