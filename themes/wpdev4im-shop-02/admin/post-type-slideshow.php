<?php
function register_post_type_slideshow_init() {
  $labels = array(
    'name' => 'Slideshows',
    'singular_name' => 'Slideshow',
    'add_new' => 'Add New',
    'add_new_item' => 'Add New Slideshow',
    'edit_item' => 'Edit Slideshow',
    'new_item' => 'New Slideshow',
    'all_items' => 'All Slideshows',
    'view_item' => 'View Slideshow',
    'search_items' => 'Search Slideshows',
    'not_found' =>  'No slideshows found',
    'not_found_in_trash' => 'No slideshows found in Trash', 
    'parent_item_colon' => '',
    'menu_name' => 'Slideshows'
  );

  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => array( 'slug' => 'slideshow' ),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => 5,
    'supports' => array( 'title', 'editor', 'thumbnail','page-attributes' )
  ); 

  register_post_type( 'slideshow', $args );
}
add_action( 'init', 'register_post_type_slideshow_init' );
?>