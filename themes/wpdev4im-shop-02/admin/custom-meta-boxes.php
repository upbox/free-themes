<?php
/**
 * Include and setup custom metaboxes and fields.
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
 */

add_filter( 'cmb_meta_boxes', 'wpdev4im_cmb_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function wpdev4im_cmb_metaboxes( array $meta_boxes ) {
	
	
	# Example Functions
	# https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress/blob/master/example-functions.php
	
$prefix = "";	
	
$meta_boxes[] = array(
		'id'         => 'product_data_metabox',
		'title'      => 'Product Data',
		'pages'      => array( 'product', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			
			array(
				'name' => 'Merchantname',
				'desc' => '',
				'id'   => $prefix . 'merchantname',
				'type' => 'text_small',
			),
			array(
				'name' => 'Category',
				'desc' => '',
				'id'   => $prefix . 'category',
				'type' => 'text_medium',
			),
			
			array(
				'name' => 'Price',
				'desc' => '',
				'id'   => $prefix . 'price',
				'type' => 'text_money',
			),
			
			array(
				'name' => 'Short Description',
				'desc' => '',
				'id'   => $prefix . 'product_short_description',
				'type' => 'textarea_small',
			),
			
			array(
				'name' => 'Linkurl',
				'desc' => '',
				'id'   => $prefix . 'linkurl',
				'type' => 'text',
			),
			
			
			array(
				'name' => 'imageurl',
				'desc' => 'Upload an image or enter an URL.',
				'id'   => $prefix . 'imageurl',
				'type' => 'file',
			),
			
		),
	);


	return $meta_boxes;
}

add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );
/**
 * Initialize the metabox class.
 */
function cmb_initialize_cmb_meta_boxes() {

	if ( ! class_exists( 'cmb_Meta_Box' ) )
		require_once get_template_directory() .'/lib/cmb/init.php';

}