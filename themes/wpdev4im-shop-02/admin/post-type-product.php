<?php

function wpdev4im_product_custom_init() {
  $labels = array(
    'name' 			=> 'Products',
    'singular_name' => 'Product',
    'add_new' 		=> 'Add New',
    'add_new_item' 	=> 'Add New Product',
    'edit_item' 	=> 'Edit Product',
    'new_item' 		=> 'New Product',
    'all_items' 	=> 'All Products',
    'view_item' 	=> 'View Product',
    'search_items' 	=> 'Search Products',
    'not_found' 	=>  'No products found',
    'not_found_in_trash' 	=> 'No products found in Trash', 
    'parent_item_colon' 	=> '',
    'menu_name' 			=> 'Products'
  );

  $args = array(
    'labels' 			 => $labels,
    'public' 			 => true,
    'publicly_queryable' => true,
    'show_ui' 			=> true, 
    'show_in_menu' 		=> true, 
    'query_var' 		=> true,
    'rewrite' 			=> array( 'slug' => 'product' ),
    'capability_type' 	=> 'post',
	//'taxonomies'          => array('post_tag' ),
    'has_archive' 		=> true, 
    'hierarchical' 		=> true,
    'menu_position' 	=> 5,
    'supports' 			=> array( 'title', 'editor')
  ); 

  register_post_type( 'product', $args );
  
  
  // Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x( 'Prouct Categories', 'taxonomy general name' ),
		'singular_name'              => _x( 'prouct-category', 'taxonomy singular name' ),
		'search_items'               => __( 'Search Categories' ),
		'popular_items'              => __( 'Popular Categories' ),
		'all_items'                  => __( 'All Categories' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Category' ),
		'update_item'                => __( 'Update Category' ),
		'add_new_item'               => __( 'Add New Category' ),
		'new_item_name'              => __( 'New Category Name' ),
		'separate_items_with_commas' => __( 'Separate categories with commas' ),
		'add_or_remove_items'        => __( 'Add or remove categories' ),
		'choose_from_most_used'      => __( 'Choose from the most used categories' ),
		'not_found'                  => __( 'No categories found.' ),
		'menu_name'                  => __( 'Prouct Categories' ),
	);

	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'product-category' ),
	);

	register_taxonomy( 'product-category', 'product', $args );
	
	 // Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x( 'Product Tags', 'taxonomy general name' ),
		'singular_name'              => _x( 'product-tag', 'taxonomy singular name' ),
		'search_items'               => __( 'Search Tags' ),
		'popular_items'              => __( 'Popular Tags' ),
		'all_items'                  => __( 'All Tags' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Tag' ),
		'update_item'                => __( 'Update Tag' ),
		'add_new_item'               => __( 'Add New Tag' ),
		'new_item_name'              => __( 'New Tag Name' ),
		'separate_items_with_commas' => __( 'Separate tags with commas' ),
		'add_or_remove_items'        => __( 'Add or remove tags' ),
		'choose_from_most_used'      => __( 'Choose from the most used tags' ),
		'not_found'                  => __( 'No tags found.' ),
		'menu_name'                  => __( 'Product Tags' ),
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'product-tag' ),
	);

	register_taxonomy( 'product-tag', 'product', $args );
	
	
}
add_action( 'init', 'wpdev4im_product_custom_init' );


