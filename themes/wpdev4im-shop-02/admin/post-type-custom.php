<?php
/*
 * Register Custom Post Type
 * Doc http://codex.wordpress.org/Class_Reference/WP_Query
 * Generate tool http://generatewp.com/post-type/
 */
  
// Register Custom Post Type
function wpdev4im_custom_post_type() {

	$labels = array(
		'name'                => _x( 'Products', 'Post Type General Name', 'wpdev4im' ),
		'singular_name'       => _x( 'Product', 'Post Type Singular Name', 'wpdev4im' ),
		'menu_name'           => __( 'Product', 'wpdev4im' ),
		'parent_item_colon'   => __( 'Parent Product:', 'wpdev4im' ),
		'all_items'           => __( 'All Products', 'wpdev4im' ),
		'view_item'           => __( 'View Product', 'wpdev4im' ),
		'add_new_item'        => __( 'Add New Product', 'wpdev4im' ),
		'add_new'             => __( 'New Product', 'wpdev4im' ),
		'edit_item'           => __( 'Edit Product', 'wpdev4im' ),
		'update_item'         => __( 'Update Product', 'wpdev4im' ),
		'search_items'        => __( 'Search products', 'wpdev4im' ),
		'not_found'           => __( 'No products found', 'wpdev4im' ),
		'not_found_in_trash'  => __( 'No products found in Trash', 'wpdev4im' ),
	);
	$args = array(
		'label'               => __( 'product', 'wpdev4im' ),
		'description'         => __( 'Product information pages', 'wpdev4im' ),
		'labels'              => $labels,
		'supports'            => array( ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'product', $args );

}

// Hook into the 'init' action
add_action( 'init', 'wpdev4im_custom_post_type', 0 );



// Register Custom Taxonomy
function wpdev4im_custom_taxonomy()  {

	$labels = array(
		'name'                       => _x( 'Genres', 'Taxonomy General Name', 'wpdev4im' ),
		'singular_name'              => _x( 'Genre', 'Taxonomy Singular Name', 'wpdev4im' ),
		'menu_name'                  => __( 'Genre', 'wpdev4im' ),
		'all_items'                  => __( 'All Genres', 'wpdev4im' ),
		'parent_item'                => __( 'Parent Genre', 'wpdev4im' ),
		'parent_item_colon'          => __( 'Parent Genre:', 'wpdev4im' ),
		'new_item_name'              => __( 'New Genre Name', 'wpdev4im' ),
		'add_new_item'               => __( 'Add New Genre', 'wpdev4im' ),
		'edit_item'                  => __( 'Edit Genre', 'wpdev4im' ),
		'update_item'                => __( 'Update Genre', 'wpdev4im' ),
		'separate_items_with_commas' => __( 'Separate genres with commas', 'wpdev4im' ),
		'search_items'               => __( 'Search genres', 'wpdev4im' ),
		'add_or_remove_items'        => __( 'Add or remove genres', 'wpdev4im' ),
		'choose_from_most_used'      => __( 'Choose from the most used genres', 'wpdev4im' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'genre', 'product', $args );

}

// Hook into the 'init' action
add_action( 'init', 'wpdev4im_custom_taxonomy', 0 );


?>