<?php
/**
 * The template for displaying image attachments.
 *
 * @package WPDev4IM Shop Default Theme
 * @since WPDev4IM Shop Default Theme 1.0
 */
?>

<?php get_header(); ?>

	<?php if (have_posts()) : ?>
	   	
		<?php while (have_posts()) : the_post(); ?>
       		<?php get_template_part( 'loop', 'image' ); ?>
	   	<?php endwhile; ?>
      
       <?php wpdev4im_content_nav( 'nav-below' ); ?>
	 
	 <?php else : ?>
    
        <?php get_template_part( 'no-results', 'index' ); ?>
    
    <?php endif; ?>
    
    <?php get_sidebar(); ?>

<?php get_footer(); ?>

